@intg
Feature: Get_Batches_Reference
    As an API consumer
    I want to query batches with reference
    So that I know they have been processed

	@get-Batches-Reference_byBatchReference
	Scenario: Perform a transaction and then settle the current set of transactions and get with reference
		Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I get all Batch Transaction Details
        Then response code should be 200
		When I get Batche Details With Reference Number
		Then response code should be 200
		And response body should contain "service":"ACH"
		And response body should contain "reference"
		And response body should contain "volume"
		And response body should contain totalItemCount"
		And Verify Get Batches 'Count' and 'Volumes' values Matching at Get Batches Reference	
		
	@get-Batches-Reference_byPageNo
    Scenario: query batch by page no 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches?pageNumber=10
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode	
		
	@get-Batches-Reference_byPageSize
    Scenario: query batch by page size 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches?pageSize=1
        Then response code should be 200
        And response header Content-Type should be application/json
		And response body should contain "pageSize":1
		And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
		
	@get-Batches-Reference_bySortDirectionAsc
    Scenario: query batch by ascending order 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches?sortsortField=Ascending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode	

	@get-Batches-Reference_bySortDirectionDesc
    Scenario: query batch by descending order 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches?sortsortField=Descending&sortField=date
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain ACH
        And response body should not contain /ACH
        And response body should not contain Bankcard
        And response body should not contain transactionCode		
	

	############  Negative Scenarios  #################		

		
	@get-Batches-Reference_inValidReference
	Scenario: Try to get Batches Reference with invalid Reference 
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
		When I use HMAC and GET /batches/ABCABCABC
		Then response code should be 404
		And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.	

		



 