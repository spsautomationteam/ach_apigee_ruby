
And(/^I set Authorization header with MID as (.*) and MKEY as (.*)$/) do |mid, mkey|
  auth ='Bearer SPS-DEV-GW:test.'+mid+'.'+mkey
  @ach.set_header 'Authorization', auth
end

And(/^I set url as (.*)$/) do |url|
  @ach.set_url url
end

And(/^I set (.*) header to (.*)$/) do |header, value|
  @ach.set_header header, value
end

And(/^I set body to (.*)$/) do |json|
  @ach.set_json_body json
end

When(/^I use HMAC and POST to (.*)$/) do |arg1|
   @ach.postHmac arg1
end

When(/^I use HMAC and PUT to (.*)$/) do |text|
  @ach.put_transaction text
end

When(/^I use HMAC and POST LineItems using lineItem Body (.*)$/) do | body |
  @ach.post_lineitems body
end

When(/^I use HMAC and POST LineItems using Invalid reference number (.*)$/) do | body |
  @ach.post_lineitems body
end

And(/^I post data for transaction (.*)$/) do |path|
  @ach.post path
end

And(/^I patch data for transaction (.*)$/) do |path|
  @ach.patch path
end

And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @ach.verify_response_headers header_param, val
end

Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @ach.verify_response_params param, val
end

Then(/^response body path (.*) must with regex (.*)$/) do |param, val|
  @ach.verify_regex param, val
end

Then(/^response body path (.*) should contain (.*)$/) do |param, val|
  @ach.verify_response_params_contain param, val
end

Then(/^response body path should contain (.*)$/) do | val |
  @ach.verify_response_value_contain  val
end

Then(/^response body should contain above performed transaction reference$/) do
  @ach.verify_response_for_refnumber
end

Then(/^(.*) response body path should contain same token number$/) do |text|
  @ach.verify_response_for_tokennumber
end

Then(/^response body path should be contain same reference number$/) do
  @ach.verify_response_contain_refnumber
end

Then(/^response body should contain the same reference number$/) do
  @ach.verify_response_contain_refnumber
end

Then(/^response body path should not contain (.*)$/) do |val|
  @ach.verify_response_value_not_contain val
end

Then(/^I get a list of existing (.*) transactions$/) do |type|
  @ach.get_listof_transactions type
end

Then(/^I get that same transaction details$/) do
  @ach.get_current_transdetails
end

Then(/^I use HMAC then GET same credit transaction details using (.*)$/) do | path |
  @ach.get_current_credit_transdetails path
end

  Then(/^I get that same transaction details using reference number$/) do
    @ach.get_current_transdetails
  end

Then(/^I get that same transaction details by using reference number and (.*)$/) do | method |
  @ach.get_current_transdetails_by_refnumber method
end

Then(/^I use HMAC and GET (.*)$/) do | val |
  @ach.get_by_paramvalue val
end

Then(/^I use HMAC then GET List of Existing Settled Batches Transactions using (.*)$/) do | path |
  @ach.get_by_paramvalue path
end

Then(/^I use HMAC then GET Batches References using (.*)$/) do | path |
  @ach.get_by_paramvalue path
end

Then(/^I use HMAC then GET List of Existing Current Settled Batches Transactions using (.*)$/) do | path |
  @ach.get_by_paramvalue path
end

Then(/^I use HMAC and then GET a list of existing (.*) transaction$/) do |type|
  @ach.get_listof_transactions type
end

Then(/^I perform a (.*) transaction$/) do |type|
  @ach.verify_response_value_contain val
end

And(/^I patch data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @ach.patch_ref path, reference
end

And(/^I post data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @ach.post_ref path, reference
end

And(/^I post data with existing ref num for transaction (.*)$/) do |path|
  @ach.post_existing_refnum path
end

And(/^I patch data with existing ref num for transaction (.*)$/) do |path|
  @ach.patch_existing_refnum path
end

Then(/^response code should be (.*)$/) do |responsecode|
  @ach.verify_response_code responsecode
end

Then(/^response is empty$/) do
  @ach.verify_response_empty
end

Then(/^I Capture the (.*) transaction with Total Amount (.*)$/) do |typeTrans, amount |
  puts "Type of Transaction for Capture #{typeTrans}"
  @ach.put_transaction amount
end

Then(/^Try to Capture the Transaction with (.*) Reference Number (.*) and Total Amount (.*)$/) do | text, path, amount |
  @ach.captureAuthorization_byInvalid_refrencenumber path, amount
end

Then(/^I use HMAC and DELETE to (.*)$/) do | path |
  @ach.delete_transaction_byreference path
end

Then(/^I use HMAC then GET lineitems to (.*)$/) do | path |
  @ach.get_current_transdetails_by_refnumber path
end

Then(/^I use HMAC and DELETE lineitems to (.*)$/) do | path |
  @ach.delete_transaction_byreference path
end

Then(/^I use POST Credits Reference using Body (.*)$/) do | body |
  @ach.post_credits_reference body
end

Then(/^I use HMAC and POST Credits Reference using Invalid reference number (.*)$/) do | body |
  @ach.post_credits_reference body
end

Then(/^I use HMAC and settle all current set of transactions using (.*)$/) do | body |
  @ach.post_credits_reference body
end
