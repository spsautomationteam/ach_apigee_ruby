@intg
Feature: Post Credits on previous perfromed Tx
	As an API consumer
	I want to post credit requests based on the previous transaction reference without knowing card number and expiration date
	So that I know they have been processed
	
	@post-credits-Reference_bySaleTxReference
    Scenario: post credit transaction from previous sale transaction reference
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Sale transaction with body set to {"amount": 10}
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.message should be ACCEPTED                        
     
	@post-credits-Reference_bySaleTxReference-WithAllTag
    Scenario: post credit transaction from previous sale transaction reference
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Sale transaction with body set to {"amount": 1,"deviceId" : "Test"}
        Then response code should be 201
        And response header Content-Type should be application/json
        And response body path $.message should be ACCEPTED	
		
		
###################### Negative scenarios ###########################

	@post-credits-Reference_byCreditTxReference
    Scenario: post credit reference transaction by credit transaction reference
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a credit transaction
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Credit transaction with body set to {"amount": 10}
        Then response code should be 404
        And response body path $.code should be 000000
		And response body path $.message should be Internal Server Error
		And response body path $.detail should be Please contact support for assistance.
		
	
	@post-credits-Reference_withOutAmountVal
    Scenario: post credit reference transaction by without passing the amount
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Sale transaction with body set to {"amount": 0}
        Then response code should be 400
        And response body path $.detail should be InvalidRequestData : INVALID T_AMT  
		
	@post-credits-Reference_InvalidAmountVal
    Scenario: post credit reference transaction by passing invalid amount
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Sale transaction with body set to {"amount": -1.90}
        Then response code should be 400
        And response body path $.detail should be InvalidRequestData : INVALID T_AMT  

	@post-credits-Reference_NullBodyData
    Scenario: post credit reference transaction by passing null body data 
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Sale transaction with body set to {}
        Then response code should be 400
        And response body path $.detail should be InvalidRequestData : request: The Amount field is required	
		
	@post-credits-Reference_NullRequest
    Scenario: post credit reference transaction from previous transaction reference without passing json body data 
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Sale transaction with body set to '        '                
        Then response code should be 400
        And response body path $.detail should be Invalid JSON request in message request, check syntax
		
	@post-credits-InvalidReference
    Scenario: post credit reference transaction from previous transaction reference by passing invalid reference 
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference with invalid reference TEST1234 with body set to {"amount": 1}           
        Then response code should be 400
        And response body path $.detail should be InvalidRequestData : INVALID T_REFERENCE
		
	@post-credits-Reference_WithTransCode
    Scenario: post credit reference transaction by Transaction Code
        Given I have valid credentials
        And I set content-type header to application/json
        When I create a sale charge
        Then response code should be 201
        And response body path $.status should be Approved
        When I perform post credit reference on the same Sale transaction with body set to {"amount": 1,"deviceId" : "Test","transactionCode": "7"}
        Then response code should be 400
		And response body path $.code should be 100007
		And response body path $.detail should be Invalid JSON request in message request, check syntax
        
				
                                
    