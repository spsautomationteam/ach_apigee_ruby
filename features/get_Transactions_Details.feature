@intg
Feature: Get Transactions Details
    As an API consumer
    I want to query transactions details requests
    So that I know they have been processed

	 @get-Transactions-Details_bySaleReference
     Scenario: query specific transaction details
       Given I set clientId header to `clientId`
       And I set merchantId header to `merchantId`
       And I set merchantKey header to `merchantKey`
       And I set Content-Type header to "application/json"
       And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
       When I use HMAC and POST to /charges?type=Sale
       Then response code should be 201
        And response body path status should be Approved
       And I get that same transaction details by using reference number and /transactions/
       Then response code should be 200
        And response header Content-Type should be application/json
        And response body path should contain "total":10
       And response body path should contain "type":"Sale"
		And response body path should not contain Bankcard
        And response body path should not contain transactionCode
        And response body path should contain Sale
        And response body path should not contain Credit
		
	@get-Transactions-Details_byCreditTransactions
    Scenario: query all credit transactions
      Given I set clientId header to `clientId`
      And I set merchantId header to `merchantId`
      And I set merchantKey header to `merchantKey`
      And I set Content-Type header to "application/json"
      And I set body to { "deviceId": "123",     "secCode": "PPD",     "originatorId": "12345",     "amounts": {         "total": 10,         "tax": 10,         "shipping": 10     },     "account": {         "type": "Checking",         "routingNumber": "056008849",         "accountNumber": "12345678901234"     },       "customer": {         "dateOfBirth": "2017-01-01",         "ssn": "123123123",         "license": {             "number": "12314515",             "stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com",         "telephone": "001111111111",         "fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name",             "last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {"token":"3e64be23d4fb405eba33544a21241729",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
      When I use HMAC and POST to /credits
      Then response code should be 201
      And response body path status should be Approved
      And I get that same transaction details by using reference number and /transactions/
      Then response code should be 200
      And response header Content-Type should be application/json
      And response body path should contain "total":10
      And response body path should contain "type":"Credit"
      And response body path should not contain Bankcard
      And response body path should not contain transactionCode
      And response body path should not contain Sale
		
	###################### Negative scenarios #########################	

	@get-Transactions-Details_byInvalidReference
     Scenario: query specific transaction details
      Given I set clientId header to `clientId`
      And I set merchantId header to `merchantId`
      And I set merchantKey header to `merchantKey`
      And I set Content-Type header to "application/json"
        When I use HMAC and GET /transactions/Test123
		Then response code should be 404
		And response body path code should be 000000
		And response body path message should be Internal Server Error
		And response body path detail should be Please contact support for assistance.

		