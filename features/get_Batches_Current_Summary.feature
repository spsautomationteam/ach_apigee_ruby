@intg
Feature: Get Batches Current Summary
    As an API consumer
    I want to query batches current summary
    So that I know they have been processed

    @get_batches_current_summary
    Scenario: query all current batch summery
        Given I set clientId header to `clientId`
        And I set merchantId header to `merchantId`
        And I set merchantKey header to `merchantKey`
        When I use HMAC and GET /batches/current/summary
        Then response code should be 200
        And response header Content-Type should be application/json
        And response body should contain paymentType
        And response body should contain authTotal
		And response body should contain saleCount
		And response body should contain saleTotal
		And response body should contain creditCount
		And response body should contain creditTotal
		And response body should contain totalCount
		And response body should contain totalVolume


