@Get_Transactions
Feature: Get_Transactions
  As an API consumer
  I want to query transactions requests
  So that I know they have been processed

  @put-token-withoutRoutingNumberTag
  Scenario: update the card data associated with a vault token without routing number tag
    Given I set clientId header to `clientId`
    And I set merchantId header to `merchantId`
    And I set merchantKey header to `merchantKey`
    And I set Content-Type header to "application/json"
    And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
    When I use HMAC and POST to /tokens
    Then response code should be 200
    And response body path should contain "status": 1
    And response body path should contain "message": "SUCCESS"
    And I set body to {"account": {"type": "Savings","accountNumber": "12345678901234"}}
    When I use HMAC and PUT to /tokens/
    Then response code should be 400
    And response body path message should be Invalid message request content
    And response body path detail should be Required content: account(routingNumber, accountNumber, type)
 #	And response body path detail should be Required content: account

