@intg
Feature:  Delete a Vault Token
	As an API consumer
	I want to delete vault token

	@delete-Token_referenced-token-AllAccountType
	Scenario Outline: delete a token generated for different account type
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": <accountType>,"routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		When I use HMAC and DELETE to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "DELETED"
		And delete response body path should contain same token number
	  # And response body path should contain "data": "daf1f1238dab43beb782e8020d252138"

		Examples:
			|accountType|
			|"Savings"|
			|"Checking"|
	
	########### Negative Scenarios #############
	
	@delete-Token_byAlready-DeletedToken
    Scenario Outline: Try to delete already delated token number
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": <accountType>,"routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		When I use HMAC and DELETE to /tokens
		Then response code should be 200
		When I use HMAC and DELETE to /tokens
        Then response code should be 404

	Examples:
	|accountType|
	|"Savings"|
	|"Checking"|


	@delete-Token_byInvalid-TokenReference
    Scenario: Try to delete token by entering wrong reference
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		When I use HMAC and DELETE to /tokens/Test123
        Then response code should be 404
		And response body path code should be 100006
		And response body path message should be Resource not found
		And response body path detail should be UNABLE TO LOCATE
		
		
	
     
	
	
    