@intg
Feature: Put Token (update the card data associated with a vault token.)
	As an API consumer
	I want to update the card data associated with a vault token.
	So that I know they can be processed

	@put-token
	Scenario Outline: update the card data associated with a vault token of different account type
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": <accountType>,"routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 200
		And put response body path should contain same token number

		Examples:
			|accountType|
			|"Savings"	|
			|"Checking" |

	@put-token-UpdateToken_Bywithout_AccountTag
	Scenario: update a card data without Account tag
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {{"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 400
		And response body path detail should be Invalid JSON request in message request, check syntax


	@put-token-withoutAccountType
	Scenario: update the card data associated with a vault token without account type
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 400
		And response body path detail should be Required content: account(routingNumber, accountNumber, type)

	@put-token-withoutAccountNumber
    Scenario: update the card data associated with a vault token without account number data
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": ""}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 400
		And response body path detail should be Required content: account(routingNumber, accountNumber, type)

	@put-token-withInvalidAccountNumber
	Scenario: update the card data associated with a vault token with invalid account number
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "1234"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 401
		And response body path should contain "code": "100008"
		And response body path should contain "message": "Invalid message request content"
		And response body path should contain "detail": "You are a using certification environment which is restricted to test data only."

	@put-token-withoutAccountNumberTag
	Scenario:update the card data associated with a vault token  without account number tag
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 400
		And response body path code should be 100008
		And response body path message should be Invalid message request content
		And response body path detail should be Required content: account(routingNumber, accountNumber, type)

	@put-token-withoutRoutingNumber
    Scenario: update the card data associated with a vault token without routing number data
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Savings","routingNumber": "","accountNumber": "12345678901234"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 400
		And response body path detail should be Required content: account(routingNumber, accountNumber, type)


	@put-token-withInvalidRoutingNumber
	Scenario: update the card data associated with a vault token with invalid routing number data
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Savings","routingNumber": "123123123","accountNumber": "12345678901234"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 401
		And response body path should contain "code": "100008"
		And response body path should contain "message": "Invalid message request content"
		And response body path should contain "detail": "You are a using certification environment which is restricted to test data only."
		
	@put-token-withoutRoutingNumberTag
    Scenario: update the card data associated with a vault token without routing number tag
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Savings","accountNumber": "12345678901234"}}
		When I use HMAC and PUT to /tokens/
		Then response code should be 400
		And response body path message should be Invalid message request content
		And response body path detail should be Required content: account(routingNumber, accountNumber, type)
		
	@put-token-withNullBodyData
    Scenario: update the card data associated with a vault token without sending request body
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {}
		When I use HMAC and PUT to /tokens/
		Then response code should be 400
		And response body path message should be Invalid message request content
		And response body path detail should be Required content: account(routingNumber, accountNumber, type)
   
	@put-token-withInvalidTokenReference
    Scenario: update the card data associated with a vault token with invalid token reference
		Given I set clientId header to `clientId`
		And I set merchantId header to `merchantId`
		And I set merchantKey header to `merchantKey`
		And I set Content-Type header to "application/json"
		And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and POST to /tokens
		Then response code should be 200
		And response body path should contain "status": 1
		And response body path should contain "message": "SUCCESS"
		And I set body to {"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"}}
		When I use HMAC and PUT to /tokens/Test123
		Then response code should be 404
		And response body path should contain "code": "100006"
		And response body path should contain "message": "Resource not found"
		And response body path should contain "detail": "UNABLE TO LOCATE"
	
		
		